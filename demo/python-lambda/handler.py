import json


def hello(event, context):
    body = {
        "message": "hello world!",
        "input": event,
    }

    return {"statusCode": 200, "body": json.dumps(body)}
